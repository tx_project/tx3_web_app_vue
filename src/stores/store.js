import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)
const login = {
    namespaced: true,
    state: {
        isLogin: false
    },
    mutations: {
        changeLoginStstus(state) {
            state.isLogin = !state.isLogin;
        }
    }
}

const store = new Vuex.Store({
    modules: {
        login
    }
});

export default store;