import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home/index'
import main from '@/components/main/main'

import hero from '@/components/main/hero/index'
import cbg from '@/components/main/cbg/index'
import gm from '@/components/main/gm/index'
import hh from '@/components/main/hh/index'
import zf from '@/components/main/zf/index'
import szzq from '@/components/main/szzq/index'


import p_main from '@/components/phone/main/index'
import phoneHome from '@/components/phone/home/index'

import p_search from '@/components/phone/main/search/index'
import p_account from '@/components/phone/main/account/index'
import p_reward from '@/components/phone/main/reward/index'
import p_shop from '@/components/phone/main/shop/index'

import p_hero from '@/components/phone/main/search/hero/index'
import p_cbg from '@/components/phone/main/search/cbg/index'
import p_gm from '@/components/phone/main/search/gm/index'
import p_hh from '@/components/phone/main/search/hh/index'
import p_zf from '@/components/phone/main/search/zf/index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/phone',
      name: 'phoneHome',
      component: phoneHome
    },
    {
      path: '/p_main',
      name: 'p_main',
      component: p_main,
      children: [
        {
          path: 'search',
          name: 'p_search',
          component: p_search
        },
        {
          path: 'account',
          name: 'p_account',
          component: p_account
        },
        {
          path: 'reward',
          name: 'p_reward',
          component: p_reward
        },
        {
          path: 'shop',
          name: 'p_shop',
          component: p_shop
        },
        {
          path: 'hero',
          name: 'p_hero',
          component: p_hero
        },
        {
          path: 'gm',
          name: 'p_gm',
          component: p_gm
        },
        {
          path: 'cbg',
          name: 'p_cbg',
          component: p_cbg
        },
        {
          path: 'hh',
          name: 'p_hh',
          component: p_hh
        },
        {
          path: 'zf',
          name: 'p_zf',
          component: p_zf
        }
      ]
    },
    {
      path:'/main',
      name: 'main',
      component: main,
      children: [
        {
          path: 'hero',
          name: 'hero',
          component: hero
        },
        {
          path: 'cbg',
          name: 'cbg',
          component: cbg
        },
        {
          path: 'gm',
          name: 'gm',
          component: gm
        },
        {
          path: 'hh',
          name: 'hh',
          component: hh
        },
        {
          path: 'zf',
          name: 'zf',
          component: zf
        },
        {
          path: 'szzq',
          name: 'szzq',
          component: szzq
        }
      ]
    }
  ]
})
