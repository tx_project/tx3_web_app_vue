// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import iView from 'iview'
import ElementUI from 'element-ui'
import Vant from 'vant'
import Vuex from 'vuex'

import store from './stores/store'

import './common/main.scss'

import 'vant/lib/vant-css/index.css'
import 'element-ui/lib/theme-chalk/index.css'
import 'iview/dist/styles/iview.css'

Vue.use(iView)
Vue.use(ElementUI)
Vue.use(Vant)


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
